Homeassignment
================ 

### Requirements
* Each POST request is forwarded to N servers.
* GET requests are forwarded to a single server, selected in a round robin fashion.
* Expose metrics for easy monitoring.
* Should be able to support N<10 servers.
* Should be able to withstand a POST request rate of 100 req/sec and GET request rate of 1000 req/sec on a reasonable machine.

### Design
A nginx server serves as a reversed proxy, to load balance the requests.  
The nginx server handle all GET requests, while POST requests are been proxied,  
to a nodejs server, which in turn deliver the POST request.  

The nginx server contain two upstreams:  
- upstream getbackend  
Lists all configured authentication servers, and handle all get request in a round robin fashion. 
- upstream postbackend  
Handle all post requests, and forward them to proxy server,  
the proxy server will then deliver the post to all the configured authenticating servers.  

### Tested environment
All installation as been made on Ubuntu 14.04 machines.
My working environment contain the following:  

#### nginx server: 

- hostname: `ec2-52-16-112-204.eu-west-1.compute.amazonaws.com:80`

- ports: 80 (http), 5000 (logstash-forwarder)

#### nodejs ("POST") server: 
 - hostname: `ec2-52-17-44-244.eu-west-1.compute.amazonaws.com:3000`
 - ports: 3000 (http)
#### authenticating servers (10 in total): 
 - hostnames:   
`ec2-54-72-190-193.eu-west-1.compute.amazonaws.com:3000`  
`ec2-52-17-250-122.eu-west-1.compute.amazonaws.com:3000`  
`ec2-54-72-204-181.eu-west-1.compute.amazonaws.com:3000`  
`ec2-54-72-34-210.eu-west-1.compute.amazonaws.com:3000`  
`ec2-54-72-94-212.eu-west-1.compute.amazonaws.com:3000`  
`ec2-52-17-204-42.eu-west-1.compute.amazonaws.com:3000`  
`ec2-54-72-98-42.eu-west-1.compute.amazonaws.com:3000`  
`ec2-52-17-255-127.eu-west-1.compute.amazonaws.com:3000`  
`ec2-52-17-247-126.eu-west-1.compute.amazonaws.com:3000`  
`ec2-54-72-35-132.eu-west-1.compute.amazonaws.com:3000`  
 - ports: 3000 (http)
 - routes:  
/register (POST)  
/changePassword (POST)    
/login (GET)  
#### ELK server:  
I use ELK on a separated to monitor logs from the nginx server (which are been delivered by logstash-forwarder).  
I will prevent from you the pleasure to set it up :) , and i will handle it internally.  
here is a [link](http://ec2-54-185-91-206.us-west-2.compute.amazonaws.com/#/discover) to Kibana dashboard. 

### Installation

For each environment do the following:  
1. clone the repo  
``` sh
git clone https://bitbucket.org/ofervelich/homeassignment.git
```
2. Go in to the cloned repo  
``` sh
cd homeassignment
```
3. run the installation script:
##### authenticating server (optional ?)  
``` sh
./appserver.sh
```
##### Nodejs 
``` sh
./node_lb.sh
```
##### Nginx  
``` sh
./nginx_lb.sh -j /full/path/to/json/config
```
The last step require a json config that will list all of the upstream servers (hostname and port number), a live example can be found under `homeassignment/nginx_lb/config.json`  
``` json
{
	"upstream_get_servers": [
		"ec2-54-72-190-193.eu-west-1.compute.amazonaws.com:3000",
		"ec2-52-17-250-122.eu-west-1.compute.amazonaws.com:3000",
		"ec2-54-72-204-181.eu-west-1.compute.amazonaws.com:3000",
		"ec2-54-72-34-210.eu-west-1.compute.amazonaws.com:3000",
		"ec2-54-72-94-212.eu-west-1.compute.amazonaws.com:3000",
		"ec2-52-17-204-42.eu-west-1.compute.amazonaws.com:3000",
		"ec2-54-72-98-42.eu-west-1.compute.amazonaws.com:3000",
		"ec2-52-17-255-127.eu-west-1.compute.amazonaws.com:3000",
		"ec2-52-17-247-126.eu-west-1.compute.amazonaws.com:3000",
		"ec2-54-72-35-132.eu-west-1.compute.amazonaws.com:3000"
	],
	"upstream_post_servers": [
		"ec2-52-17-44-244.eu-west-1.compute.amazonaws.com:3000"
	]
}
```