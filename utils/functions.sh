#!/bin/bash

function execute {
	echo "running command: $@" | tee -a ~/installation.log
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $@" >&2
        exit $status
    fi
}