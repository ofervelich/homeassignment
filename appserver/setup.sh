#!/bin/bash

# include the common functions
# =================================================
. $(pwd)/../utils/functions.sh
# =================================================

execute sudo apt-get update
execute sudo apt-get --quiet --yes install nodejs
execute sudo apt-get --quiet --yes install npm
execute sudo npm install forever -g
execute sudo chown -R $(whoami) ~/.npm
execute sudo chown -R $(whoami) ~/tmp

if [[ ! -L /usr/bin/node ]]; then
	execute sudo ln -s /usr/bin/nodejs /usr/bin/node
fi

execute sudo chown -R $(whoami) ~/tmp

pushd ~/homeassignment/appserver
	execute npm install	
    execute forever start server.js
popd