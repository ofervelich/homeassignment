var express = require('express');
var app = express();

app.get('/', function (req, res) {
	console.log('ok');
	res.send('ok!');
});

app.all('/register', function (req, res) {
	res.send('you have been register! (server 1)');
});

app.all('/changePassword', function (req, res) {
	res.send('you have changed your password! (server 1)');
});

app.get('/login', function (req, res) {
	res.send('login ok! (server 1)');
});

var server = app.listen(3000, function () {

	var host = server.address().address;
	var port = server.address().port;

	console.log('App listening at http://%s:%s', host, port);

});