#!/bin/bash


# how to ....
# =================================================
function usage {
	SCRIPT_NAME=`basename $1`
	echo Setup nginx load balancer script
	echo
	echo Usage:
	echo     $SCRIPT_NAME -j path/to/json
	echo Example:
	echo     $SCRIPT_NAME -j /home/ubuntu/config.json
}

# get the script parameters
# =================================================
json=
while getopts "j:h" OPTION
do
	case $OPTION in
		h)
			usage $0
			exit 1
			;;
		j)
			json=$OPTARG
			;;
		?)
			usage $0
			exit
			;;
     esac
done

if [ -z ${json} ]; then
	usage $0
	exit 1
fi

# run
# =================================================
pushd nginx_lb
        ./setup.sh ${json}
popd