#!/bin/bash

# include the common functions
# =================================================
. $(pwd)/../utils/functions.sh
# =================================================


# install nginx and configure host
# =================================================
execute sudo apt-get update
execute sudo apt-get --quiet --yes install nginx
execute sudo apt-get --quiet --yes install ruby

ruby configure.rb $1

execute sudo cp ./mydefault /etc/nginx/sites-available/

pushd /etc/nginx/sites-enabled
	execute sudo rm default
	execute sudo ln -s ../sites-available/mydefault default
popd

execute sudo service nginx restart

# install logstash forwarder
# =================================================
echo 'deb http://packages.elasticsearch.org/logstashforwarder/debian stable main' | sudo tee /etc/apt/sources.list.d/logstashforwarder.list
wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | sudo apt-key add -
execute sudo apt-get update
execute sudo apt-get --quiet --yes install logstash-forwarder

execute sudo cp ./logstash-forwarder.conf /etc/logstash-forwarder.conf
execute sudo service logstash-forwarder start
execute sudo mkdir -p /etc/pki/tls/certs
execute sudo chmod 664 ./logstash-forwarder.crt
execute sudo cp ./logstash-forwarder.crt /etc/pki/tls/certs/

execute sudo service logstash-forwarder start