#!/usr/bin/env ruby

require 'json'
require 'erb'

json_config = ARGV[0]

file = File.read(json_config)

hash = JSON.parse(file)

upstream_get_servers = hash['upstream_get_servers']
upstream_post_servers = hash['upstream_post_servers']
allhosts = hash['upstream_get_servers'].join(' ')

template_path = File.expand_path("../mydefault.erb", __FILE__)

template = File.read(template_path)

File.open('mydefault', 'w+') do |f|
	f.write(ERB.new(template).result(binding))
end