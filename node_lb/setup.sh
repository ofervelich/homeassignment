#!/bin/bash

# include the common functions
# =================================================
. $(pwd)/../utils/functions.sh
# =================================================

execute sudo apt-get update
execute sudo apt-get --quiet --yes install nodejs
execute sudo apt-get --quiet --yes install npm
execute sudo npm install forever -g
execute sudo chown -R $(whoami) ~/.npm

if [[ ! -L /usr/bin/node ]]; then
	execute sudo ln -s /usr/bin/nodejs /usr/bin/node
fi

pushd ~/homeassignment/node_lb
        execute forever start lb.js
popd