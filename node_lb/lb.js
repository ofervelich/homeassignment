var http = require("http");
var url = require("url");
var request = require('request');

function onRequest(req, res) {

	// the request uri path 
    var pathname = url.parse(req.url).pathname;

    // list of all hosts
    var hosts = req.headers.allhosts.split(' ');

    // make a request and wait for reply
    //
    var reply = function(data) {
        request.post(data, function (error, response, body) {
            if(!error) {
                res.writeHead(response.statusCode, response.headers);
                res.write(body);
                res.end();
            } else {
                console.log("We have an error:", error)
            }
        });
    }

    // construct the request data
    //
    var requestData = function(host) {
        return {
            headers: req.headers,
            url: 'http://'+ host + pathname
        };
    }

    // run POST to all N servers
    for (var i = 1; i < hosts.length; i++) {
        request.post(requestData(hosts[i]));
    };

    // 
    reply(requestData(hosts[0]));

}

http.createServer(onRequest).listen(3000);
console.log("Server has started.");